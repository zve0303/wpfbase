﻿using System.Windows.Controls;
using ViewModels;
using BaseWPFApp;

namespace Views
{
    /// <summary>
    /// Interaction logic for MainWindowUserControl.xaml
    /// </summary>
    public partial class View2 : UserControl
    {
        public View2()
        {
            InitializeComponent();
            DataContext = new ViewModel2();

        }
    }
}
