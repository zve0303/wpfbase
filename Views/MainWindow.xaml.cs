﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using ViewModels;

namespace Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainWindowViewModel();
            (DataContext as MainWindowViewModel).MenuMustClose += CloseMenu;
        }

        private void Get_WIPFolderViewPart(object sender, RoutedEventArgs e)
        {
            DataContext = new ViewModel1();
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {

        }

        private void DemoItemsListBox_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MenuToggleButton.IsChecked = false;
        }

       

        private void CloseMenu(object sender, object e)
        {
            MenuToggleButton.IsChecked = false;
        }
    }
}
