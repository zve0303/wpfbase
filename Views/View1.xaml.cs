﻿using System.Windows.Controls;
using ViewModels;

namespace Views
{
    /// <summary>
    /// Interaction logic for MainWindowUserControl.xaml
    /// </summary>
    public partial class View1 : UserControl
    {
        public View1()
        {
            InitializeComponent();
            DataContext = new ViewModel1();

        }

        
    }
}
