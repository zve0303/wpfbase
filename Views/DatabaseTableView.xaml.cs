﻿using System.Windows.Controls;
using ViewModels;
using BaseWPFApp;

namespace Views
{
    /// <summary>
    /// Interaction logic for MainWindowUserControl.xaml
    /// </summary>
    public partial class DatabaseTableView : UserControl
    {
        public DatabaseTableView(ViewModel2 vm2)
        {
            InitializeComponent();
            DataContext = vm2;

        }
    }
}
