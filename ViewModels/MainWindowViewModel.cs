﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using BaseWPFApp;
using Views;

namespace ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        
        public EventHandler MenuMustClose;

        public UserControlItem[] Items { get; set; }

        
        private UserControlItem _previousSelectedItem;
        private UserControlItem _selectedItem;
        public UserControlItem SelectedItem
        {
            get => _selectedItem;
            set => this.MutateVerbose(ref _selectedItem, value, RaisePropertyChanged());
        }

        public MainWindowViewModel()
        {
            Items = new[]
            {
                new UserControlItem("View1", new View1()),
                new UserControlItem("Data",new View2()),
            };
            SelectedItem = Items.First();
        }

    }
}
