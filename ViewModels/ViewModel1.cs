﻿using System;
using System.ComponentModel;
using BaseWPFApp;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows;

namespace ViewModels
{
    public class ViewModel1:ViewModelBase
    {

        #region properties
        private string _Drawingnumber;
        public string Drawingnumber
        {
            get => _Drawingnumber;
            set => this.MutateVerbose(ref _Drawingnumber, value, RaisePropertyChanged());
        }


        #endregion
        public ViewModel1()
        {
            DoSomethingCommand = new RelayCommand(DoSomething);
            
        }

        #region Commands
        public ICommand DoSomethingCommand { get; set; }
        #endregion

        public void DoSomething()
        {

        }

    }
}
