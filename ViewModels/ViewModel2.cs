﻿using BaseWPFApp;
using Models;
using Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Database;

namespace ViewModels
{
    public class ViewModel2: ViewModelBase
    {
        private ObservableCollection<Person> _people;

        public ObservableCollection<Person> People
        {
            get => _people;
            set => this.MutateVerbose(ref _people, value, RaisePropertyChanged());
    }
    private UserControlItem _dbtableview;
        private UserControlItem _createview;
        private UserControlItem _updateview;
        private UserControlItem _selecteditem;
        public UserControlItem SelectedItem
        {
            get => _selecteditem;
            set => this.MutateVerbose(ref _selecteditem, value, RaisePropertyChanged());
        }
        private Person _selectedperson;
        public Person SelectedPerson
        {
            get => _selectedperson;
            set => this.MutateVerbose(ref _selectedperson, value, RaisePropertyChanged());
        }
        private Person _newperson;
        public Person NewPerson
        {
            get => _newperson;
            set => this.MutateVerbose(ref _newperson, value, RaisePropertyChanged());
        }
        public ViewModel2()
        {
            People = new ObservableCollection<Person>();
            NewPerson = new Person();
            SelectedPerson = new Person();
            Refresh();

            CreateCmd = new RelayCommand(Create);
            UpdateCmd = new RelayCommand(Update);
            DeleteCmd = new RelayCommand(Delete);
            CancelCmd = new RelayCommand(Cancel);
            CreateItemCmd = new RelayCommand(CreateItem);
            UpdateItemCmd = new RelayCommand(UpdateItem);

            _dbtableview = new UserControlItem("dbtableview", new DatabaseTableView(this));
            _createview = new UserControlItem("createview", new DatabaseCreateView(this));
            _updateview = new UserControlItem("updateview", new DatabaseUpdateView(this));
            SelectedItem = _dbtableview;
        }

        #region Commands
        public ICommand CreateCmd { get; set; }
        public ICommand UpdateCmd { get; set; }
        public ICommand DeleteCmd { get; set; }
        public ICommand RefreshCmd { get; set; }
        public ICommand CancelCmd { get; set; }

        public ICommand CreateItemCmd { get; set; }
        public ICommand UpdateItemCmd { get; set; }
        #endregion

        #region Methods
        public void Create()
        {
            SelectedItem = _createview;       
        }
        public void Update()
        {
            SelectedItem = _updateview;
        }
        public void Delete()
        {
            SqliteDataAccess.Delete(SelectedPerson);
            Refresh();
        }
        public void Refresh()
        {
            People =SqliteDataAccess.LoadPeople();
            SelectedPerson = new Person();
            NewPerson = new Person();
        }
        public void Cancel()
        {
            Refresh();
            SelectedItem = _dbtableview;
        }
        public void CreateItem()
        {
            SqliteDataAccess.Create(NewPerson);
            Refresh();
            SelectedItem = _dbtableview;
        }
        public void UpdateItem()
        {
            SqliteDataAccess.Update(SelectedPerson);
            Refresh();
            SelectedItem = _dbtableview;
        }
        #endregion
    }
}
