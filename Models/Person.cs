﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using BaseWPFApp;

namespace Models
{
    public class Person:ViewModelBase
    {
        // If you want to bind to properties, you need to implement interface
        private int _id;
        public int Id
        {
            get => _id;
            set => this.MutateVerbose(ref _id, value, RaisePropertyChanged());
        }
        private string _firstname;
        public string Firstname
        {
            get => _firstname;
            set => this.MutateVerbose(ref _firstname, value, RaisePropertyChanged());
        }
        private string _lastname;
        public string Lastname
        {
            get => _lastname;
            set => this.MutateVerbose(ref _lastname, value, RaisePropertyChanged());
        }
    }
}
