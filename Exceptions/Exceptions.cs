﻿using System;

namespace CustomExceptions
{
    public class FileNotFoundException : Exception
    {
        public string message;
        public FileNotFoundException()
        { }
        public FileNotFoundException(string message)
        { this.message = message; }
    }
    public class DatabaseException : Exception
    {
        public string message;
        public DatabaseException()
        { }
        public DatabaseException(string message)
        { this.message = message; }
    }
    public class ValueMissingException : Exception
    {
        public string message;
        public ValueMissingException()
        { }
        public ValueMissingException(string message)
        { this.message = message; }
    }
    public class MailNotSentException : Exception
    {
        public string message;
        public MailNotSentException()
        { }
        public MailNotSentException(string message)
        { this.message = message; }
    }
    public class PrintException : Exception
    {
        public string message;
        public PrintException()
        { }
        public PrintException(string message)
        { this.message = message; }
    }

}
