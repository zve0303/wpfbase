﻿using System;
using System.Windows.Input;

namespace BaseWPFApp
{
    /// <summary>
    /// Parameterless command. To be used: SelectWIPFolderCommand = new RelayCommand(SelectWIPFolder) (in ctor)
    /// in which selectwipfoldercommand is a public ICommand prop with getter and setter that contain an ICommand private prop
    /// </summary>
    public class RelayCommand : ICommand
    {
        public Action _execute;

        private Predicate<object> _canExecute;

        private event EventHandler CanExecuteChangedInternal;

        public RelayCommand(Action execute) : this(execute, DefaultCanExecute)
        {
        }

        public RelayCommand(Action execute, Predicate<object> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException("execute");
            _canExecute = canExecute ?? throw new ArgumentNullException("canExecute");
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
                CanExecuteChangedInternal += value;
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
                CanExecuteChangedInternal -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute != null && _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute();
        }

        public void OnCanExecuteChanged()
        {
            var handler = CanExecuteChangedInternal;
            handler?.Invoke(this, EventArgs.Empty);
        }

        public void Destroy()
        {
            _canExecute = _ => false;
            _execute = null;
        }

        private static bool DefaultCanExecute(object parameter)
        {
            return true;
        }
    }
}

