﻿using System;
using System.ComponentModel;

namespace BaseWPFApp
{
    /// <summary>
    /// This class is used as base for User Controls. Usage: Items = new []
    //{
    //    new UserControlItem("Home", new HomeControl(this)),
    //    new UserControlItem("Populate from Simulator", new PopulateFromSimulatorControl(this)),
    //    new UserControlItem("Search by EAN", new SearchInfeedControl(this))
    //};
    /// </summary>
    public class UserControlItem : INotifyPropertyChanged
    {

        public UserControlItem(string name, object content)
        {
            Name = name;
            Content = content;
        }

        private string _name;
        public string Name
        {
            get => _name;
            set => this.MutateVerbose(ref _name, value, RaisePropertyChanged());
        }

        private object _content;
        public object Content
        {
            get => _content;
            set => this.MutateVerbose(ref _content, value, RaisePropertyChanged());
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private Action<PropertyChangedEventArgs> RaisePropertyChanged()
        {
            return args => PropertyChanged?.Invoke(this, args);
        }
    }
}
