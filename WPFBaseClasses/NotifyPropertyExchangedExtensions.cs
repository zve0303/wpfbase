﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;
using System.Threading.Tasks;

namespace BaseWPFApp
{   
        public static class NotifyPropertyChangedExtension
        {
            public static void MutateVerbose<TField>(this INotifyPropertyChanged instance, ref TField field,
                TField newValue, Action<PropertyChangedEventArgs> raise, [CallerMemberName] string propertyName = null)
            {
                field = newValue;
                raise?.Invoke(new PropertyChangedEventArgs(propertyName));
            }
        }
}
