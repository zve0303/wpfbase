﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using CustomExceptions;
using Models;

namespace Database
{
    public class SqliteDataAccess
    {
        #region Connectionstring
        private static string LoadConnectionString()
        {
            var conn = @"Data Source= Database\Database.db;Version=3;";
            return conn;
        }
        #endregion

        #region GET
        public static ObservableCollection<Person> LoadPeople()
        {
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
                {
                    var output = cnn.Query<Person>("select * from People order by Firstname ASC", new DynamicParameters());
                    var obscol = new ObservableCollection<Person>(output);
                    return obscol;
                }

            }
            catch (Exception e)
            {
                throw new DatabaseException("Could not load Companies. Error: " + e.Message);
            }

        }
        #endregion

        #region Create
        public static int NewIdPeople()
        {
            try
            {

                int id;
                using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
                {
                    var output = cnn.Query<int>("select Max(Id) from People", new DynamicParameters());
                    ObservableCollection<int> obsCollection = new ObservableCollection<int>(output);
                    id = obsCollection.First()+1;
                }
                return id;
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        public static void Create(Person person)
        {
            try
            {
                person.Id = NewIdPeople();
                using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
                {
                    cnn.Execute("insert into People (Id,Firstname,Lastname) values (@Id,@Firstname,@Lastname)", person);
                }
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Checkexistence
        //public static bool ExistsInDb(CompanyModel company)
        //{
        //    try
        //    {

        //        if (company != null)
        //        {
        //            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
        //            {
        //                var rows = cnn.Query(string.Format(
        //                "SELECT COUNT(1) as 'Count' FROM Company WHERE Id = '{0}'",
        //                company.Id));

        //                return (int)rows.First().Count > 0;
        //            }
        //        }
        //        else
        //        {
        //            return false;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        throw new DatabaseException("Could not verify existence of " +
        //            company.Name + ". Error: " + e.Message);
        //    }

        //}
        #endregion

        #region Delete

        public static void Delete(Person person)
        {
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
                {
                    cnn.Execute("DELETE FROM People WHERE Id = @Id", person);
                }
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region Update

        public static void Update(Person person)
        {
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
                {
                    cnn.Execute("UPDATE People SET " +
                    "Firstname = @Firstname, " +
                    "Lastname = @Lastname " +
                    "WHERE Id = @Id", person);
                }               
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion


    }
}

